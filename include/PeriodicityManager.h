#ifndef _PERIODICITY_MANAGER_
#define _PERIODICITY_MANAGER_
#include "tmfe_fegem.h"
#include "GEM_BANK.h"
#include <mutex>
class PeriodicityManager {
private:
   int fPeriod;
   int fNumberOfConnections;
   std::vector<std::string> RemoteCallers;
   TMFeEquipment *fEq;
   TMFE *fMfe;
   std::mutex *fgMutex;
   int fPeriodicWithData;
   int fPeriodicWithoutData;

   int fGEMBankEvents;
   double fGEMStatLastTime;

   int fODBReads;

   MVOdb *fOdbStatistics;

   // Used to track how much time it has been since we saw data
   std::chrono::time_point<std::chrono::system_clock> TimeOfLastData;

public:
   PeriodicityManager(TMFE *mfe, TMFeEquipment *eq, std::mutex *mutex);
   std::string ProgramName(std::string string);
   void AddRemoteCaller(std::string prog);
   void LogPeriodicWithData();
   void LogPeriodicWithoutData();
   void LogODBRead();

   void AddBanksProcessed(int nbanks);
   void WriteGEMBankStatistics();

   void UpdatePerodicity();
   void ProcessMessage(GEMBANK<char> *bank);
   int GetWaitPeriod() const { return fPeriod; }

   double SecondsSinceData()
   {
      std::chrono::time_point<std::chrono::system_clock> timer_start = std::chrono::high_resolution_clock::now();
      std::chrono::duration<double> TimeSinceLastStatusUpdate = timer_start - TimeOfLastData;
      return TimeSinceLastStatusUpdate.count();
   }
};

#endif
