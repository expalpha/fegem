#include "feGEMClass.h"
#include "feGEMWorker.h"

#ifndef FEGEM_SUPERVISOR_
#define FEGEM_SUPERVISOR_
#include <algorithm>    // std::remove_if
class feGEMSupervisor : public feGEMClass {
public:
   MVOdb *fOdbWorkers;
   std::vector<std::string> fHostlist = {"local_host"};
   std::vector<std::string> fIplist = {"unset_entry"};

   int fPortRangeStart;
   int fPortRangeStop;

   feGEMSupervisor(TMFE *mfe, TMFeEquipment *eq, std::mutex * mutex);

   void Init();

private:
   std::vector<uint> RunningWorkers;

public:
   bool WorkerIsRunning(uint workerID)
   {
      for (const auto &id : RunningWorkers) {
         if (id == workerID) {
            ScreenMessage("Worker is already runnning");
            return true;
         }
      }
      ScreenMessage("Working is not yet runnning");
      return false;
   }
   void WorkerStarted(uint workerID) { RunningWorkers.push_back(workerID); }
   virtual int FindHostInWorkerList(const std::string hostname, const std::string ip_address);
   virtual uint16_t AssignPortForWorker(uint workerID);
   std::string BuildFrontendName(const std::string hostname);
   virtual const char *AddNewClient(const std::string hostname, const std::string ip_address);

   virtual void SetFEStatus()
   {
      size_t threads = RunningWorkers.size();
      std::string status =
         "" + fMfe->fFrontendName + "@" + fMfe->fFrontendHostname + " [" + std::to_string(threads) + " thread";
      if (threads > 1)
         status += "s";
      status += "]";
      fEq->SetStatus(status.c_str(), "greenLight");
   }
};

#endif
