#ifndef FEGEM_CLASS_
#define FEGEM_CLASS_

#include <thread>
#include <mutex>
#include "midas.h"
#include "tmfe_fegem.h"
#include "msystem.h"

#include "GEM_BANK.h"
#include "MessageHandler.h"
#include "AllowedHosts.h"
#include "HistoryLogger.h"
#include "PeriodicityManager.h"

#include "mvodb.h"
#include <set>

class feGEMClass : public TMFeRpcHandlerInterface, public TMFePeriodicHandlerInterface {
public:
   TMFE *fMfe;
   TMFeEquipment *fEq;
   std::mutex*  fgMutex;
   enum RunStatusType { Unknown, Running, Stopped };
   enum feGEMClassEnum { SUPERVISOR, WORKER, INVALID };
   const int feGEMClassType;
   // TCP stuff
   int fServer_fd;
   
   struct sockaddr fAddress;
   int addrlen = sizeof(fAddress);

   // Bound listen address
   struct sockaddr_in fListen_address;

   // Incoming IP address lookup
   struct sockaddr_in fAddress_info;
   int addr_in_len = sizeof(fAddress_info);

   int fPort;
   std::thread fTCPThread;
   // TCP read rate:
   PeriodicityManager fPeriodicity;

   int fEventSize;
   int fLastEventSize; // Used to monitor any changes to fEventSize
   char *fEventBuf;
   // JSON Verbosity control
   int fDebugMode;

   std::string fThisHostname;

   // Elog posting settings
   std::string fElogHost = "";
   int fMaxElogPostSize; // Used to put a safetly limit on the size of an elog post

   std::chrono::time_point<std::chrono::system_clock> fLastStatusUpdate;

   // Periodic task query items (sould only be send from worker class... not yet limited)
   RunStatusType fRunStatus;
   int fRUNNO;
   uint32_t fRUN_START_T;
   uint32_t fRUN_STOP_T;
   std::set<std::string> fODBQueries;

   // Network security
   AllowedHosts *fAllowedHosts;

   // JSON reply manager
   MessageHandler fMessage;

   HistoryLogger fHistoryLogger;
   feGEMClass(TMFE *mfe, TMFeEquipment *eq, AllowedHosts *hosts, std::mutex* mutex, int type, int debugMode = 0);
   ~feGEMClass();

   virtual int FindHostInWorkerList(const std::string hostname, const std::string ip_address) = 0;
   virtual uint16_t AssignPortForWorker(uint workerID) = 0;
   virtual const char *AddNewClient(const std::string hostname, const std::string ip_address) = 0;

   std::string HandleRpc(const char *cmd, const char *args);
   void HandleBeginRun();
   void HandleEndRun();
   void HandleStrBank(GEMBANK<char> *bank, const std::string hostname, const std::string ip_address);
   void HandleFileBank(GEMBANK<char> *bank, const std::string hostname, const std::string ip_address);
   void HandleStrArrayBank(GEMBANK<char> *bank);
   void HandleCommandBank(const GEMDATA<char> *bank, const char *command, const std::string hostname, const std::string ip_address);
   void PostElog(GEMBANK<char> *bank, const std::string hostname, const std::string ip_address);
   void LogBank(const char *buf, const std::string hostname, const std::string ip_address);
   std::pair<int,int> HandleBankArray(const char *ptr, const std::string hostname, const std::string ip_address);
   int HandleBank(const char *ptr, const std::string hostname, const std::string ip_address);

   void SetFEStatus(int seconds_since_last_post = 0);

   void HandlePeriodic(){};
   void ServeHost();
   void Run();
   void ScreenMessage(const char *format, ...)
   {
      va_list arglist;
      if (feGEMClassType == WORKER)
         printf("\t");
      printf("[%s] ", fEq->fName.c_str());

      // printf( "Error: " );
      va_start(arglist, format);
      vprintf(format, arglist);
      va_end(arglist);
      printf("\n");
   }
};

#endif
