
#ifndef _MessageHandler_
#define _MessageHandler_

#include "midas.h"
#include "tmfe.h"
#include <vector>
#include <string>
#include <iostream>
#include <string.h>
#include <assert.h>
#include <mutex>

class MessageHandler {
private:
   TMFE *fMfe;
   std::mutex fMsgLock;
   std::vector<std::vector<char>> fJSONMessageQueue;
   std::vector<std::string> fJSONErrorQueue;
   int fTotalText;

public:
   MessageHandler(TMFE *mfe);
   ~MessageHandler();
   bool HaveErrors() { 
     std::lock_guard<std::mutex> lock(fMsgLock);
     return fJSONErrorQueue.size();
   };
   void QueueData(const char* name, int data);
   // Queue String and protect against nested '"'
   void QueueString(const char *name, const char *msg, int length = -1);
   // Queue JSON object
   void QueueFormattedData(const char *name, std::vector<std::string> names, std::vector<std::string> data);
   void QueueMessage(const char *msg);
   void QueueError(const char *source, const char *err);
   std::vector<char> ReadMessageQueue(double midas_time);
public:
   static std::string ProtectNestedQuotes(const char* data, int length = -1)
   {
      std::string message;
      if (length < 0)
         length = strlen(data);
      message.reserve(length);
      for (int i = 0; i < length; i++) {
         if (data[i] == '"')
            message.push_back('\\');
         message.push_back(data[i]);
      }
      return message;
   }
};

#endif
