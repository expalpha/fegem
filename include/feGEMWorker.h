#include "feGEMClass.h"

#ifndef FEGEM_WORKER_
#define FEGEM_WORKER_

class feGEMWorker : public feGEMClass {
public:
   MVOdb *fOdbSupervisorSettings;
   feGEMWorker(TMFE *mfe, TMFeEquipment *eq, AllowedHosts *hosts, std::mutex* mutex, const std::string client_hostname, int debugMode = 0);
   void Init(MVOdb *supervisor_settings_path);

   // Functions only for feGEMSupervisor
   virtual int FindHostInWorkerList(const std::string hostname, const std::string ip_address)
   {
      // Intentionally fail
      assert(0);
      // Supress compiler warnings
      assert(hostname.size());
      assert(ip_address.size());
      return -1;
   };
   virtual uint16_t AssignPortForWorker(uint workerID)
   {
      // Intentionally fail
      assert(0);
      // Supress compiler warnings
      assert(workerID);
      return -1;
   };
   virtual const char *AddNewClient(const std::string hostname, const std::string ip_address)
   {
      // Intentionally fail
      assert(0);
      // Supress compiler warnings
      assert(hostname.size());
      assert(ip_address.size());
      return "";
   }
};

#endif
