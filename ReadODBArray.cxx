
#include "odbxx.h"

class OdbArrayReader
{
   private:
      midas::odb fOdb;
      int64_t fLastRead; // Converted to UXIXTime
      bool IsValidPath;
   OdbArrayReader(std::string Category, std::string Variable)
   {
      std::string path = "/" + Category + "/Variable/" + Variable;
      fOdb(path);
   }
   std::string Read()
   {
      return fOdb.dump();
   }
}
