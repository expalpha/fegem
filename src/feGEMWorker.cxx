//
// feGEM.cxx
//
// Frontend for two way communication to labVIEW (and or python)
// JTK McKENNA
//
#include "feGEMWorker.h"

//--------------------------------------------------
// Base class for LabVIEW frontend
// Child classes:
//    feGEM supervisor (recieves new connections and refers a host to a worker)
//    feGEM worker (one worker per host)
//--------------------------------------------------

feGEMWorker::feGEMWorker(TMFE *mfe, TMFeEquipment *eq, AllowedHosts *hosts, std::mutex* mutex, const std::string client_hostname, int debugMode)
   : feGEMClass(mfe, eq, hosts, mutex, WORKER, debugMode)
{
   fMfe = mfe;
   fEq = eq;
   fHistoryLogger.SetClientHostname(client_hostname);
   // Default event size ok 10kb, will be overwritten by ODB entry in Init()
   fEventSize = 10000;
   fEventBuf = NULL;
   SetFEStatus();

   // Default size limit on elog posts is 1MB
   fMaxElogPostSize = 1000000;

   // Creating socket file descriptor
   fServer_fd = socket(AF_INET, SOCK_STREAM, 0);
   fcntl(fServer_fd, F_SETFL, O_NONBLOCK);
   if (fServer_fd == 0)
      exit(1);
   // Forcefully attaching socket to the port 5555
   int opt = 1;
   if (setsockopt(fServer_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) {
      // perror("setsockopt");
      exit(1);
   }

   fRunStatus = Unknown;
   fRUNNO = -1;
   fRUN_START_T = 0;
   fRUN_STOP_T = 0;
   std::lock_guard<std::mutex> lock(*fgMutex);
   fMfe->fOdbRoot->RI("Runinfo/Run number", &fRUNNO);
   fMfe->fOdbRoot->RU32("Runinfo/Start Time binary", &fRUN_START_T);
   fMfe->fOdbRoot->RU32("Runinfo/Stop Time binary", &fRUN_STOP_T);

   // int period=1000;
   // fEq->fOdbEqCommon->RI("Period",&period);
   // fEq->fCommon->Period=period;
}
void feGEMWorker::Init(MVOdb *supervisor_settings_path)
{
   fOdbSupervisorSettings = supervisor_settings_path;
   fMfe->Msg(MINFO, fEq->fName.c_str(), "Initialising %s...", fEq->fName.c_str());
   usleep(100);
   std::lock_guard<std::mutex> lock(*fgMutex);
   // Set the path of feGEM as the default path for settings database
   const int bufsize = 200;
   char buf[bufsize] = {0};
   readlink("/proc/self/exe", buf, bufsize);

   // Remove everything after last forwardslash (ie /feGEM.exe)
   for (int i = bufsize; i > 0; i--) {
      if (buf[i] == '/') {
         buf[i] = '\0';
         break;
      }
   }

   fEq->fOdbEqSettings->RI("event_size", &fEventSize, true);

   // Get (and set defaults if needed) the elog settings and limits
   fEq->fOdbEqSettings->RI("elog_post_size_limit", &fMaxElogPostSize, true);
   fOdbSupervisorSettings->RS("elog_hostname", &fElogHost, true);

   SetFEStatus();

   fLastEventSize = fEventSize;
   fEq->fOdbEqSettings->WS("feVariables", "No variables logged yet...", 32);
   fEq->fOdbEqSettings->WU32("DateAdded", 0);
   assert(fPort > 0);
   if (fEventBuf) {
      free(fEventBuf);
   }
   fEventBuf = (char *)malloc(fEventSize);

   ScreenMessage("Binding to: %d", fPort);

   // Maybe bind to just one IP address?
   // Probably bad news since we don't re-init a running worker thread
   fListen_address.sin_family = AF_INET;
   fListen_address.sin_addr.s_addr = INADDR_ANY;
   fListen_address.sin_port = htons(fPort);

   // Forcefully attaching socket to the port fPort
   if (bind(fServer_fd, (struct sockaddr *)&fListen_address, sizeof(fListen_address)) < 0) {
      ScreenMessage("Failed to bind");
      exit(1);
   }
   ScreenMessage("Bound");
   // assert (rc==0);
   fTCPThread = std::thread(&feGEMClass::Run, this);
}
