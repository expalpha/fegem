//
// feGEM.cxx
//
// Frontend for two way communication to labVIEW (and or python)
// JTK McKENNA
//
#include "feGEMClass.h"
#include <cmath>        // std::isnan, std::isinf
#include <sstream>      // std::ostringstream

// Amazing, C++ doesn't have a built in case insensitive string compare...
// I built this quickly, its not super optimised
// Amazing, C++ doesn't have a built in case insensitive string compare...
// I built this quickly, its not super optimised
static bool case_insensitive_compare(const std::string& a, const std::string& b)
{
   // Lengths differ... so contents differs...
   if (a.size() != b.size() )
      return false;
   const size_t len = a.size();
   for (size_t i = 0; i < len; i++)
      if ( std::tolower(a[i]) != std::tolower(b[i]))
         return false;
   return true;
}
static bool case_insensitive_n_compare(const std::string& a, const std::string& b, const size_t len)
{
   // Lengths differ... so contents differs...
   if (a.size() < len )
      return false;
   if (b.size() < len )
      return false;
   for (size_t i = 0; i < len; i++)
      if ( std::tolower(a[i]) != std::tolower(b[i]))
         return false;
   return true;
}
//--------------------------------------------------
// Base class for LabVIEW frontend
// Child classes:
//    feGEM supervisor (recieves new connections and refers a host to a worker)
//    feGEM worker (one worker per host)
//--------------------------------------------------

feGEMClass::feGEMClass(TMFE *mfe, TMFeEquipment *eq, AllowedHosts *hosts, std::mutex *mutex, int type, int debugMode)
   : feGEMClassType(type), fPeriodicity(mfe, eq, mutex), fMessage(mfe), fHistoryLogger(mfe, eq)
{
   fgMutex = mutex;
   fAllowedHosts = hosts;
   fDebugMode = debugMode;
   char hostname[100];
   gethostname(hostname, 100);
   // Store as std::string in this class
   fThisHostname = std::string(hostname);
   // Hostname must be known!
   assert(fThisHostname.size() > 0);
   fLastStatusUpdate = std::chrono::high_resolution_clock::now();
}

feGEMClass::~feGEMClass() // dtor
{
   SetFEStatus(-1);
   fTCPThread.join();
   if (fEventBuf) {
      free(fEventBuf);
      fEventBuf = NULL;
   }
}

std::string feGEMClass::HandleRpc(const char *cmd, const char *args)
{
   std::lock_guard<std::mutex> lock(*fgMutex);
   fMfe->Msg(MINFO, "HandleRpc", "RPC cmd [%s], args [%s]", cmd, args);
   return "OK";
}

void feGEMClass::HandleBeginRun()
{
   std::lock_guard<std::mutex> lock(*fgMutex);
   fMfe->Msg(MINFO, "HandleBeginRun", "Begin run!");
   fMfe->fOdbRoot->RI("Runinfo/Run number", &fRUNNO);
   fMfe->fOdbRoot->RU32("Runinfo/Start Time binary", &fRUN_START_T);
   // Stop time gets reset to 0 (1/1/1970) at start run
   // fMfe->fOdbRoot->RU32("Runinfo/Stop Time binary", &RUN_STOP_T);
   // fEq->SetStatus("Running", "#00FF00");
   fRunStatus = Running;
}

void feGEMClass::HandleEndRun()
{
   fMfe->Msg(MINFO, "HandleEndRun", "End run!");
   std::lock_guard<std::mutex> lock(*fgMutex);
   fMfe->fOdbRoot->RI("Runinfo/Run number", &fRUNNO);
   fMfe->fOdbRoot->RU32("Runinfo/Start Time binary", &fRUN_START_T);
   fMfe->fOdbRoot->RU32("Runinfo/Stop Time binary", &fRUN_STOP_T);
   // fEq->SetStatus("Stopped", "#00FF00");
   fRunStatus = Stopped;
}
void feGEMClass::SetFEStatus(int seconds_since_last_post)
{
   std::string status = "Rate limit set: " + std::to_string(fEventSize / 1000) + "KiB/s";
   // The History logger sets negative time in its deconstructor (to set all status's grey when the frontend closes)
   if (seconds_since_last_post < 0) {
      fEq->SetStatus("feGEM thread stopped", "mgray");
   } else if (seconds_since_last_post < 100) {
      fEq->SetStatus(status.c_str(), "greenLight");
   } else if (seconds_since_last_post < 300) {
      status += " [No data for ~" + std::to_string(seconds_since_last_post) + " s]";
      fEq->SetStatus(status.c_str(), "greenLight");
   } else if (seconds_since_last_post < 3600) {
      status += " [No data for ~" + std::to_string((int)(seconds_since_last_post / 60)) + " mins]";
      fEq->SetStatus(status.c_str(), "yellowLight");
   } else {
      status += " [No data for ~" + std::to_string((int)(seconds_since_last_post / 3600)) + " hours]";
      fEq->SetStatus(status.c_str(), "redLight");
   }
}

template<class T>
std::string vector_to_string_array( std::vector<T> values)
{
   std::ostringstream a;
   a << "[";
   const size_t size = values.size();
   for (size_t i = 0; i < size; i++)
   {
      // LabVIEW needs Not a Number as 'NaN' not 'nan'
      // LabVIEW needs infinity as 'Infinity' not 'inf'
      const T& value = values.at(i);
      if (std::isnan(value)) {
         a << "NaN";
      } else if (std::isinf(value)) {
         if (value < 0)
            a << "-";
         a << "Infinity";
      } else {
         a << std::scientific   << value;
      }
      if ( i == size - 1)
         a << "]";
      else
         a << ',';
   }
   return a.str();
}

template<>
std::string vector_to_string_array<std::string>( std::vector<std::string> values)
{
   std::string a = "[";
   const size_t size = values.size();
   for (size_t i = 0; i < size; i++)
   {
      a += MessageHandler::ProtectNestedQuotes(values.at(i).c_str());
      if ( i == size - 1)
         a += "]";
      else
         a += ',';
   }
   return a;
}

template<>
std::string vector_to_string_array<bool>( std::vector<bool> values)
{
   std::string a = "[";
   const size_t size = values.size();
   for (size_t i = 0; i < size; i++)
   {
      if (values.at(i))
         a += "true";
      else
         a += "false";
      if ( i == size - 1)
         a += "]";
      else
         a += ',';
   }
   return a;
}


void feGEMClass::HandleCommandBank(const GEMDATA<char> *bank, const char *command, const std::string hostname,
                                   const std::string ip_address)
{

   // Add 'Commands' that can be sent to feGEM, they either change something or get a response back.
   ScreenMessage("COMMAND %s from: %s (%s)", command, hostname.c_str(), ip_address.c_str());

   // Banks associated with a new commection (1/4)
   if (strncmp(command, "START_FRONTEND", 14) == 0) {
      assert(feGEMClassType == SUPERVISOR);
      // If the local hostname and DNS entry do not match, prefer the
      // DNS entry (many cloned virtual machines have the name of thier
      // parent image)
      if (strcmp(bank->DATA(0), hostname.c_str()) != 0 && hostname.size()) {
         fMessage.QueueString("FrontendStatus", AddNewClient(hostname, ip_address));
      } else {
         fMessage.QueueString("FrontendStatus", AddNewClient(bank->DATA(0), ip_address));
      }
      return;
   }
   // Banks associated with a new commection (2/4)
   else if (strncmp(command, "ALLOW_HOST", 14) == 0) {
      if (!fAllowedHosts->SelfRegistrationIsAllowed()) {
         fMfe->Msg(
            MINFO, "feGEM",
            "LabVIEW host name %s tried to register its self on allowed host list, but self registration is disabled",
            hostname.c_str());
         return;
      }
      std::string reportedname(bank->DATA(0));
      std::string dnsname(hostname);
      //Trim to first '.' to remove domain name
      if (reportedname.find('.') < reportedname.size() )
         reportedname = reportedname.substr(0,reportedname.find('.'));
      if (dnsname.find(',') < dnsname.size() )
         dnsname = dnsname.substr(0,dnsname.find('.'));
      //strcasecmp is a linux thing from string.h, microsoft has stricmp
      if (case_insensitive_compare(reportedname, dnsname)) {
      //if (strcmp(bank->DATA(0), hostname.c_str()) != 0) {
         fMfe->Msg(MINFO, "feGEM", "LabVIEW host name %s does not match DNS lookup %s (%s)", bank->DATA(0), hostname.c_str(),
                   ip_address.c_str());
         fAllowedHosts->AddHost(hostname, ip_address);
      }
      fAllowedHosts->AddHost(hostname, ip_address);
      return;
   }
   // Banks associated with a new commection (3/4)
   // Send the address for data to be logged to, assumes client uses the DNS
   else if (strncmp(command, "GIVE_ME_ADDRESS", 15) == 0) {
      assert(feGEMClassType == SUPERVISOR);
      fMessage.QueueString("SendToAddress", fThisHostname.c_str());
      return;
   }
   // Banks associated with a new commection (4/4)
   else if (strncmp(command, "GIVE_ME_PORT", 12) == 0) {
      assert(feGEMClassType == SUPERVISOR);

      int WorkerNo;
      // A cloned virtual machine will miss report its hostname (it will
      // think it has the cloned machines hostname, not the new hostname)
      std::string reportedname(bank->DATA(0));
      std::string dnsname(hostname);
      //Trim to first '.' to remove domain name
      if (reportedname.find('.') < reportedname.size() )
         reportedname = reportedname.substr(0,reportedname.find('.'));
      if (dnsname.find(',') < dnsname.size() )
         dnsname = dnsname.substr(0,dnsname.find('.'));
      //strcasecmp is a linux thing from string.h, microsoft has stricmp
      if (case_insensitive_compare(reportedname, dnsname)) {
      //if (strcmp(bank->DATA(0), hostname.c_str()) != 0 && hostname.size()) {
         fMfe->Msg(MTALK, "feGEM", "LabVIEW host name %s does not match DNS lookup %s", bank->DATA(0), hostname.c_str());
         WorkerNo = FindHostInWorkerList(hostname, ip_address);
      } else {
         WorkerNo = FindHostInWorkerList(hostname, ip_address);
      }
      int port = AssignPortForWorker(WorkerNo);
      char log_to_port[80];
      sprintf(log_to_port, "%u", port);
      // std::cout<<"SEND TO PORT:"<<port<<std::endl;
      ScreenMessage("%s (%s): Log to port: %u", hostname.c_str(), ip_address.c_str(), port);
      //This port number must be a string (backwards compatible to older feGEM LabVIEW implementations)
      fMessage.QueueString("SendToPort", log_to_port);
      return;
   }

   else if (strncmp(command, "ALLOW_SSH_TUNNEL", 16) == 0) {
      if (!fAllowedHosts->SelfRegistrationIsAllowed()) {
         fMfe->Msg(
            MINFO, "feGEM",
            "LabVIEW host name %s tried to register its self on allowed host list, but self registration is disabled",
            hostname.c_str());
         return;
      }
      // We do not know the name of the host yet... they will reconnect on a new port...
      // and, for example, lxplus might have a new alias)
      fAllowedHosts->AddHost(hostname, ip_address);
      return;
   }
   // Note all devices have their time correctly set (Many CRIO's)
   // Note: Some devices don't have their own clocks... (arduino)... so lets support them too!
   else if (strncmp(command, "CHECK_TIME_SYNC", 15) == 0) {
      using namespace std::chrono;
      milliseconds ms = duration_cast<milliseconds>(system_clock::now().time_since_epoch());
      ScreenMessage("Time stamp for comparison: %f", (double)ms.count() / 1000. + 2082844800.);
      fMessage.QueueString("LV_TIME_NOW", std::to_string((double)ms.count() / 1000. + 2082844800. ).c_str());
      return;
   }
   // Enable verbose JSON replies. DebugMode is tracked with an int for future flexibility
   else if (strncmp(command, "ENABLE_DEBUG_MODE", 17) == 0) {
      assert(feGEMClassType == WORKER);
      fDebugMode = 1;
      return;
   } else if (strncmp(command, "DISABLE_DEBUG_MODE", 18) == 0) {
      assert(feGEMClassType == WORKER);
      fDebugMode = 0;
      return;
   }
   // Commonly used command, every connection is going to ask for the MIDAS every buffer size
   else if (strncmp(command, "GET_EVENT_SIZE", 14) == 0) {
      return fMessage.QueueString("EventSize", std::to_string(fEventSize).c_str());
   } else if (strncmp(command, "GET_RUNNO", 9) == 0) {
      return fMessage.QueueData("RunNumber", fRUNNO);
   } else if (strncmp(command, "GET_RUN_START_T", 15) == 0) {
      return fMessage.QueueData("RunStartTime", fRUN_START_T);
   } else if (strncmp(command, "GET_RUN_STOP_T", 14) == 0) {
      return fMessage.QueueData("RunStopTime", fRUN_STOP_T);
   } else if (strncmp(command, "GET_STATUS", 10) == 0) {
      switch (fRunStatus) {
         case Unknown: fMessage.QueueString("RunStatus", "UNKNOWN"); break;
         case Running: fMessage.QueueString("RunStatus", "RUNNING"); break;
         case Stopped: fMessage.QueueString("RunStatus", "STOPPED"); break;
      }
      return;
   } else if (strncmp(command, "SET_EVENT_SIZE", 14) == 0) {
      assert(feGEMClassType == WORKER);
      ScreenMessage("Updating event size: %s", bank->DATA(0));
      int new_size = atoi(bank->DATA(0));
      if (new_size > fEventSize)
         fEventSize = new_size;
      if (fEventSize < 10000) {
         fMfe->Msg(MTALK, "feGEM", "Minimum event size can not be less that 10 kilo bytes");
         fEventSize = 10000;
      }
      std::lock_guard<std::mutex> lock(*fgMutex);
      fEq->fOdbEqSettings->WI("event_size", fEventSize);
      ScreenMessage("Event size updated to: %d", fEventSize);
      SetFEStatus();
      return;
   } else if (strncmp(command,"ODB_QUERY",9) == 0) {
      assert(feGEMClassType == WORKER);
      //fMessage.QueueData("ODB_QUERY:",bank->DATA(0));
      std::string odbvar(bank->DATA(0));
      // Strip any proceeding '/' from the variable name
      while (odbvar.front() == '/')
         odbvar.erase(0,1);
      if ( ! case_insensitive_n_compare(odbvar,"equipment",9) &&
           !  case_insensitive_n_compare(odbvar,"runinfo",7) )
      {
         std::string warning = "Reading the ODB for anything other than 'runinfo' and 'equipment' is forbidden... dont read: " +odbvar;
         std::cout << warning <<std::endl;
         return fMessage.QueueError("ODB_QUERY",warning.c_str());
      }

      if (!fODBQueries.count(odbvar))
         fODBQueries.insert(odbvar);
      else
         return;

      fPeriodicity.LogODBRead();

      std::string json_title = std::string("ODB_QUERY");
      int tid, num_values, total_size, item_size, last_written;
      MVOdbError error;
      {
      std::lock_guard<std::mutex> lock(*fgMutex);
      fMfe->fOdbRoot->ReadKey(odbvar.c_str(), &tid, &num_values, &total_size,&item_size, &error);
      }
      std::vector<std::string> keys;
      std::vector<std::string> data;
/*      
      std::cout << error.fError << std::endl;
      std::cout << error.fErrorString << std::endl;
      std::cout << error.fPath <<std::endl; // odb path corresponding to the error
      std::cout << error.fStatus << std::endl;
      std::cout <<"TID: " <<tid <<std::endl;

      //When there is an error:
      1
      MIDAS db_find_key() at ODB path "/Runinfo/RunNumber" returned status 312
      /Runinfo/RunNumber
      312
      TID: 0
      */
      if (error.fError)
      {
         fMessage.QueueString(json_title.c_str(), error.fErrorString.c_str());
         return;
      }
      {
      std::lock_guard<std::mutex> lock(*fgMutex);
      fMfe->fOdbRoot->ReadKeyLastWritten(odbvar.c_str(),&last_written, &error);
      }
      keys.push_back("varname"); data.push_back(std::string("\"") + odbvar + std::string("\""));
      keys.push_back("TID");     data.push_back(std::to_string(tid));
      
      keys.push_back("type"); 
      switch (tid)
      {
         case TID_BOOL:
         {
            data.push_back("\"TID_BOOL\"");
            break;
         }
         case TID_DOUBLE:
         {
            data.push_back("\"TID_DOUBLE\"");
            break;
         }
         case TID_FLOAT:
         {
            data.push_back("\"TID_FLOAT\"");
            break;
         }
         case TID_STRING:
         {
            data.push_back("\"TID_STRING\"");
            break;
         }
         /*case TID_INT16:
         {
            data.push_back("\"TID_INT16\"");
            break;
         }*/
         case TID_UINT16:
         {
            data.push_back("\"TID_UINT16\"");
            break;
         }
         case TID_INT32:
         {
            data.push_back("\"TID_INT32\"");
            break;
         }
         case TID_UINT32:
         {
            data.push_back("\"TID_UINT32\"");
            break;
         }
         default:
         {
            std::cout << "Unknown case type" << std::endl;
            fMessage.QueueString(json_title.c_str(),"UNKNOWN DATA TYPE");
            return;
         }
      }

      keys.push_back("last_written");   data.push_back(std::to_string(last_written));

      keys.push_back("value");
      std::lock_guard<std::mutex> lock(*fgMutex);
      switch (tid)
      {
         case TID_BOOL:
         {
            std::vector<bool> values;
            fMfe->fOdbRoot->RBA(odbvar.c_str(), &values); // TID_BOOL
            data.push_back(vector_to_string_array<bool>(values));
            break;
         }
         case TID_DOUBLE:
         {
            std::vector<double> values;
            fMfe->fOdbRoot->RDA(odbvar.c_str(),&values ); // TID_INT
            data.push_back(vector_to_string_array<double>(values));
            break;
         }
         case TID_FLOAT:
         {
            std::vector<float> values;
            fMfe->fOdbRoot->RFA(odbvar.c_str(),&values ); // TID_INT
            data.push_back(vector_to_string_array<float>(values));
            break;
         }
         case TID_STRING:
         {
            std::vector<std::string> values;
            fMfe->fOdbRoot->RSA(odbvar.c_str(),&values ); // TID_STRING
            data.push_back(vector_to_string_array<std::string>(values));
            break;

         }
         /*case TID_INT16:
         {
            std::vector<uint16_t> values;
            fMfe->fOdbRoot->RU16A(odbvar.c_str(),&values ); // TID_WORD)
            data.push_back(vector_to_string_array<uint16_t>(values));
            break;

         }*/
         case TID_UINT16:
         {
            std::vector<uint16_t> values;
            fMfe->fOdbRoot->RU16A(odbvar.c_str(),&values ); // TID_WORD)
            data.push_back(vector_to_string_array<uint16_t>(values));
            break;
         }
         case TID_INT32:
         {
            std::vector<int> values;
            fMfe->fOdbRoot->RIA(odbvar.c_str(),&values ); // TID_INT
            data.push_back(vector_to_string_array<int>(values));
            break;
         }
         case TID_UINT32:
         {
            std::vector<uint32_t> values;
            fMfe->fOdbRoot->RU32A(odbvar.c_str(),&values ); // TID_DWORD
            data.push_back(vector_to_string_array<uint32_t>(values));
            break;
         }
         default:
         {
            std::cout << "Unknown case type" << std::endl;
            fMessage.QueueString(json_title.c_str(),"UNKNOWN DATA TYPE");
            break;

         }
      }
      if (!keys.empty() && keys.size() == data.size())
         return fMessage.QueueFormattedData("ODB_QUERY",keys,data);

      return;

   } else {
      ScreenMessage("Command not understood!");
      bank->print(strlen(bank->DATA(0)), 2, 0, true);
   }
   return;
}

void feGEMClass::HandleFileBank(GEMBANK<char> *bank, const std::string hostname, const std::string ip_address)
{
   // LabVIEW code is setup so that only one file is sent in a bank...
   ScreenMessage("File recieved from %s (%s)", hostname.c_str(), ip_address.c_str());
   ScreenMessage("File of ~ %d bytes",bank->GetTotalSize());
   ScreenMessage("No special handing of files! It only goes into the .mid files");
   // const GEMDATA<char> *gem_data = bank->GetFirstDataEntry();
   // const char *file_data = gem_data->DATA(0);
   // ScreenMessage("File data: %s");

   return;
}

void feGEMClass::HandleStrArrayBank(GEMBANK<char> *bank)
{
   if (strncmp(bank->NAME.VARCATEGORY, "CLIENT_INFO", 14) == 0) {
      std::vector<std::string> array;

      uint32_t entries = bank->BlockSize - bank->GetDataEntry(0)->GetHeaderSize();

      std::string line;
      for (uint32_t i = 0; i < entries; i++) {
         const char c = *(bank->GetDataEntry(0)->DATA(i));
         if (c)
            line.push_back(c);
         else// if (!c)
         {
            if (line.size())
               array.push_back(line);
            line.clear();
         }
           
      }

      std::string message = "Writing \"";
      size_t max_len = 0;
      for (std::string line : array) {
         message += line + "\n";
         if (line.size() > max_len)
            max_len = line.size();
      }
      message += "\" into ";
      for (size_t i = 0; i < sizeof(bank->NAME.VARNAME); i++)
         message += bank->NAME.VARNAME[i];
      message += " in equipment settings";
      ScreenMessage("%s", message.c_str());
      {
      std::lock_guard<std::mutex> lock(*fgMutex);
      fEq->fOdbEqSettings->WSA(bank->NAME.VARNAME, array, max_len + 1);
      }
      return;
   } else {
      ScreenMessage("String array not understood!");
      bank->print();
   }
}

void feGEMClass::HandleStrBank(GEMBANK<char> *bank, const std::string hostname, const std::string ip_address)
{
   //bank->print();
   if (strncmp(bank->NAME.VARNAME, "COMMAND", 7) == 0) {
      const char *command = bank->NAME.EquipmentType;
      for (uint32_t i = 0; i < bank->NumberOfEntries; i++) {
         HandleCommandBank(bank->GetDataEntry(i), command, hostname, ip_address);
      }
      return;
   } else if (strncmp(bank->NAME.VARNAME, "TALK", 4) == 0) {
      bank->print();
      fMfe->Msg(MTALK, fEq->fName.c_str(), bank->GetDataEntry(0)->DATA(0));
      // if (strncmp(bank->NAME.VARCATEGORY,"THISHOST",8)==0)
      //{
      fPeriodicity.ProcessMessage(bank);
      //}
      return;
   } else if (strncmp(bank->NAME.VARCATEGORY, "CLIENT_INFO", 14) == 0) {

      std::string message = "Writing \"";
      message += bank->GetDataEntry(0)->DATA(0);
      message += "\" into ";
      for (size_t i = 0; i < sizeof(bank->NAME.VARNAME); i++)
         message += bank->NAME.VARNAME[i];
      message += " in equipment settings";
      ScreenMessage("%s", message.c_str());
      {
      std::lock_guard<std::mutex> lock(*fgMutex);
      fEq->fOdbEqSettings->WS(bank->NAME.VARNAME, bank->GetDataEntry(0)->DATA(0));
      }
      return;
   } else if (strncmp(bank->NAME.VARCATEGORY, "ELOG", 4) == 0) {
      if ((int)bank->GetTotalSize() > fMaxElogPostSize) {
         fMfe->Msg(MTALK, "feGEM:elog", "ELOG post too large, increase hard limit in ODB");
         return;
      }
      ScreenMessage("Posting elog");
      PostElog(bank, hostname, ip_address);
      return;
   } else if (strncmp(bank->NAME.VARCATEGORY,"MESSAGELOG",10 ) == 0) {
      std::string title(bank->NAME.VARNAME,0,16);
      std::string message(bank->GetFirstDataEntry()->DATA(0) );
      if (message.size() > 1024)
      {
         message.resize(1024);
         cm_msg1(MINFO,title.c_str(),"feGEM","The following string was too long (more than 1024 chars...) It will be trunkated:");
      }
      std::cout <<"Posting message to " << message << " log: " << message <<std::endl;

      cm_msg1(MINFO, title.c_str(), "feGEM",message.c_str());
      return;
   } else {
      ScreenMessage("String not understood!");
      bank->print();
   }
   fHistoryLogger.Update(bank);
}

void feGEMClass::PostElog(GEMBANK<char> *bank, const std::string hostname, const std::string ip_address)
{
   if (fElogHost.size() == 0) {
      fMfe->Msg(MTALK, "feGEM:elog", "No Host for the elog set... please update ODB for %s (%s)", hostname.c_str(), ip_address.c_str());
      return;
   }

   ScreenMessage("File detected");
   std::string ElogType, Encoding, Message;
   std::vector<std::string> Attribute;
   char *d = (char *)bank->GetDataEntry(0)->DATA(0);
   // Set limit many items to look for in file header
   for (int i = 0; i < 10; i++) {
      if (strncmp(d, "ELOG_TYPE:", strlen("ELOG_TYPE:")) == 0)
         ElogType = d + strlen("ELOG_TYPE:");

      else if (strncmp(d, "ATTRIBUTE:", strlen("ATTRIBUTE:")) == 0)
         Attribute.push_back(d + strlen("ATTRIBUTE:"));

      else if (strncmp(d, "ELOG_ENCODING:", strlen("ELOG_ENCODING:")) == 0)
         Encoding = d + strlen("ELOG_ENCODING:");

      else if (strncmp(d, "ELOG_MESSAGE:", strlen("ELOG_MESSAGE:")) == 0)
         Message = d + strlen("ELOG_MESSAGE:");

      // Do we have all args
      if (ElogType.size() && Encoding.size() && Message.size())
         break;
      while (*d != 0)
         d++;
      d++;
   }
   // Add escaped quote marks for each attribute (we are pipeing this commant through an ssh tunnel)
   for (std::string &arr : Attribute) {
      size_t equal_symbol_position = arr.find('=');
      arr.insert(equal_symbol_position + 1, "\\\"");
      arr.append("\\\"");
   }

   ScreenMessage("\tELOG_TYPE: %s", ElogType.c_str());
   for (std::string &arr : Attribute)
      ScreenMessage("\tATTRIBUE: %s", arr.c_str());
   ScreenMessage("\tELOG_ENCODING: %s", Encoding.c_str());
   ScreenMessage("\tELOG_MESSAGE: %s", Message.c_str());

   // Send elog:
   std::string cmd = std::string("ssh -x  ") + fElogHost + std::string(" ~/packages/elog/elog -h localhost -p 8083 ") +
                     std::string(" -l ") + ElogType + std::string(" -n ") + Encoding + std::string(" -a Run=") +
                     std::to_string(fRUNNO);
   for (std::string &att : Attribute)
      cmd += std::string(" -a ") + att;
   ScreenMessage("Command: %s", cmd.c_str());
   // snprintf(cmd, 40959, "ssh -x  %s ~/packages/elog/elog -h localhost -p 8080 -l %s -a Run=%d", fElogHost.c_str(),
   // ElogType.c_str()  gRunNumber);
   FILE *fp = popen(cmd.c_str(), "w");
   if (fp) {
      // printf("to pipe: %s\n", Message.c_str());
      fputs(Message.c_str(), fp);
      pclose(fp);
   } else {
      printf("error opening pipe\n");
   }
   return;
}

void feGEMClass::LogBank(const char *buf, const std::string hostname, const std::string ip_address)
{
   GEMBANK<void *> *ThisBank = (GEMBANK<void *> *)buf;
   if (fDebugMode) {
      char buf[200];
      sprintf(buf, "Received: %.16s/%.16s - %dx ( %d x %.4s ) [%d bytes]", ThisBank->NAME.VARCATEGORY,
              ThisBank->NAME.VARNAME, ThisBank->NumberOfEntries,
              ThisBank->GetFirstDataEntry()->GetEntries(ThisBank->BlockSize), ThisBank->NAME.DATATYPE,
              ThisBank->GetTotalSize());
      fMessage.QueueMessage(buf);
   }
   // std::cout<<ThisBank->NAME.VARNAME<<std::endl;
   if (strncmp(ThisBank->NAME.DATATYPE, "DBL", 3) == 0) {
      GEMBANK<double> *bank = (GEMBANK<double> *)buf;
      // bank->print();
      fHistoryLogger.Update(bank);
   } else if (strncmp(ThisBank->NAME.DATATYPE, "FLT", 3) == 0) {
      GEMBANK<float> *bank = (GEMBANK<float> *)buf;
      fHistoryLogger.Update(bank);
   } else if (strncmp(ThisBank->NAME.DATATYPE, "BOOL", 4) == 0) {
      GEMBANK<bool> *bank = (GEMBANK<bool> *)buf;
      fHistoryLogger.Update(bank);
      // Not supported by ODB
      /*} else if (strncmp(ThisBank->NAME.DATATYPE,"I64",3)==0) {
         GEMBANK<int64_t>* bank=(GEMBANK<int64_t>*)buf;
         logger.Update(bank);
      } else if (strncmp(ThisBank->NAME.DATATYPE,"U64",3)==0) {
         GEMBANK<uint64_t>* bank=(GEMBANK<uint64_t>*)buf;
         logger.Update(bank);*/
   } else if (strncmp(ThisBank->NAME.DATATYPE, "I32", 3) == 0) {
      GEMBANK<int32_t> *bank = (GEMBANK<int32_t> *)buf;
      fHistoryLogger.Update(bank);
   } else if (strncmp(ThisBank->NAME.DATATYPE, "U32", 4) == 0) {
      GEMBANK<uint32_t> *bank = (GEMBANK<uint32_t> *)buf;
      // bank->print();
      fHistoryLogger.Update(bank);
      // Not supported by ODB
      /*} else if (strncmp(ThisBank->NAME.DATATYPE,"I16",3)==0) {
         GEMBANK<int16_t>* bank=(GEMBANK<int16_t>*)buf;
         logger.Update(bank);*/
   } else if (strncmp(ThisBank->NAME.DATATYPE, "U16", 3) == 0) {
      GEMBANK<uint16_t> *bank = (GEMBANK<uint16_t> *)buf;
      fHistoryLogger.Update(bank);
      /*} else if (strncmp(ThisBank->NAME.DATATYPE,"I8",2)==0) {
         GEMBANK<int8_t>* bank=(GEMBANK<int8_t>*)buf;
         logger.Update(bank);
      } else if (strncmp(ThisBank->NAME.DATATYPE,"U8",2)==0) {
         GEMBANK<uint8_t>* bank=(GEMBANK<uint8_t>*)buf;
         logger.Update(bank);*/
   } else if (strncmp(ThisBank->NAME.DATATYPE, "STRA", 4) == 0) {
      GEMBANK<char> *bank = (GEMBANK<char> *)buf;
      HandleStrArrayBank(bank);
      return;
   } else if (strncmp(ThisBank->NAME.DATATYPE, "STR", 3) == 0) {
      GEMBANK<char> *bank = (GEMBANK<char> *)buf;
      HandleStrBank(bank, hostname, ip_address);
      return;
   } else if (strncmp(ThisBank->NAME.DATATYPE, "FILE", 4) == 0) {
      GEMBANK<char> *bank = (GEMBANK<char> *)buf;
      HandleFileBank(bank, hostname, ip_address);
      return;
   } else {
      ScreenMessage("Unknown bank data type... ");
      // ThisBank->print();
      std::string type;
      for (int i = 0; i < 4; i++) {
         if (ThisBank->NAME.DATATYPE[i])
            type += ThisBank->NAME.DATATYPE[i];
      }

      if (type.empty())
         type = "NULL";
      std::lock_guard<std::mutex> lock(*fgMutex);
      cm_msg(MTALK, "feGEM", "Unknown data ( type %s): %.16s/%.16s from %s (%d bytes thrown away)", type.c_str(),
             ThisBank->NAME.VARCATEGORY, ThisBank->NAME.VARNAME, hostname.c_str(), ThisBank->BlockSize - 16);

      cm_msg(MERROR, "feGEM", "Unknown data ( type %s): %.16s/%.16s from %s (%d bytes thrown away)", type.c_str(),
             ThisBank->NAME.VARCATEGORY, ThisBank->NAME.VARNAME, hostname.c_str(), ThisBank->BlockSize - 16);
      return;
   }
   return;
}

std::pair<int,int> feGEMClass::HandleBankArray(const char *ptr, const std::string hostname, const std::string ip_address)
{
   int NGEMBanks = 0;
   //std::cout<<"BLOOP"<<hostname<<std::endl;
   GEMBANKARRAY *array = (GEMBANKARRAY *)ptr;
   if (array->GetTotalSize() > (uint32_t)fEventSize) {
      char error[100];
      sprintf(error, "ERROR: [%s] More bytes sent (%u) than MIDAS has assiged for buffer (%u)", fEq->fName.c_str(),
              array->BlockSize + array->GetHeaderSize(), fEventSize);
      fMessage.QueueError(fEq->fName.c_str(), error);
      return {-1,-1};
   }
   // array->print();
   char *buf = array->GetFirstGEMBank();
   for (uint32_t i = 0; i < array->NumberOfEntries; i++) {
      GEMBANK<double> *bank = (GEMBANK<double> *)buf;
      //bank->printheader();
      int nbanks = HandleBank(buf, hostname, ip_address);
      if (nbanks > 0) // filter out errors
         NGEMBanks += nbanks;
      buf += bank->GetHeaderSize() + bank->BlockSize * bank->NumberOfEntries;
   }
   return {array->NumberOfEntries, NGEMBanks };
}

int feGEMClass::HandleBank(const char *ptr, const std::string hostname, const std::string ip_address)
{
   // Use invalid data type to probe the header
   GEMBANK<void *> *ThisBank = (GEMBANK<void *> *)ptr;
   // ThisBank->print();
   if (ThisBank->BlockSize + ThisBank->GetHeaderSize() > (uint32_t)fEventSize) {
      char error[100];
      sprintf(error, "ERROR: [%s] More bytes sent (%u) than MIDAS has assiged for buffer (%d)", fEq->fName.c_str(),
              ThisBank->GetTotalSize(), fEventSize);
      fMessage.QueueError(fEq->fName.c_str(), error);
      return -1;
   }
   size_t catlen = strlen(ThisBank->NAME.VARCATEGORY);
   size_t varlen = strlen(ThisBank->NAME.VARNAME);
   if (catlen == 0 && varlen ==0)
   {
      char buf[100];
      sprintf(buf,"Blank variable and category being logged from %s",hostname.c_str());
      std::lock_guard<std::mutex> lock(*fgMutex);
      fMfe->Msg(MERROR,fEq->fName.c_str(),buf);
      return -1;
   }
   if (catlen == 0)
   {
      char buf[100];
      sprintf(buf,"Unnammed variable with name %.16s has no category from host %s",ThisBank->NAME.VARNAME,hostname.c_str());
      std::lock_guard<std::mutex> lock(*fgMutex);
      fMfe->Msg(MERROR,fEq->fName.c_str(),buf);
      return -1;
   }
   if (varlen == 0)
   {
      char buf[100];
      sprintf(buf,"Unnammed variable with category %.16s from %s",ThisBank->NAME.VARCATEGORY, hostname.c_str());
      std::lock_guard<std::mutex> lock(*fgMutex);
      fMfe->Msg(MERROR,fEq->fName.c_str(),buf);
      return -1;
   }

   LogBank(ptr, hostname, ip_address);
   return ThisBank->NumberOfEntries;
}

void feGEMClass::Run()
{
   // std::cout<<"Run..."<<std::endl;
   while (!fMfe->fShutdownRequested) {
      ServeHost();
      // std::cout<<"Sleeping: "<<fPeriodicity.GetWaitPeriod()<<"ms"<<std::endl;
      std::this_thread::sleep_for(std::chrono::milliseconds(fPeriodicity.GetWaitPeriod()));
   }
}

void feGEMClass::ServeHost()
{
   //
   // if (fPort!=5555)
   // printf("Thread %s, periodic!\n", TMFE::GetThreadId().c_str());
   // std::cout<<"periodic (port:"<<fPort<<")"<<std::endl;
   std::chrono::time_point<std::chrono::system_clock> timer_start = std::chrono::high_resolution_clock::now();

   // Check if we need to change the MIDAS event buffer size
   if (fLastEventSize != fEventSize) {
      ScreenMessage("fEventSize updated! Flushing buffer");
      if (fEventBuf) {
         free(fEventBuf);
      }
      fEventBuf = (char *)malloc(fEventSize);
      ScreenMessage("Event buffer re-initialised ");
   }
   fLastEventSize = fEventSize;

   // Every 30s, Update the frontend status
   if (feGEMClassType == WORKER) {
      std::chrono::duration<double> TimeSinceLastStatusUpdate = timer_start - fLastStatusUpdate;

      // Update the status less frequently that the actual data
      if (TimeSinceLastStatusUpdate.count() > 30) {
         SetFEStatus((int)fPeriodicity.SecondsSinceData());
         fLastStatusUpdate = timer_start;
      }
   }

   // Listen for TCP connections
   if (listen(fServer_fd, 3) < 0) {
      perror("listen");
      exit(EXIT_FAILURE);
   }

   int new_socket = accept(fServer_fd, (sockaddr*)&fAddress, (socklen_t *)&addrlen);

   // printf("%s\n", inet_ntoa(saddr->sin_addr));
   if (new_socket < 0) {
      fPeriodicity.LogPeriodicWithoutData();
      // perror("accept");
      // exit(EXIT_FAILURE);
      //ScreenMessage("Accept failure");
      close(new_socket);
      return;
   }

   // Aquire IP address... if we are a worker, we can avoid DNS lookups
   //getsockname(new_socket, (sockaddr *)&address_info, (socklen_t *)&addr_in_len);
   std::string ip_address; //= inet_ntoa(address.sa_data);
   
       if (((sockaddr*)&fAddress)->sa_family == AF_INET) {
          ip_address = inet_ntoa(((struct sockaddr_in*)&fAddress)->sin_addr);
       }
       else
       {
          /*char host[255]={0};
          inet_ntop(
             AF_INET6,
             (((struct sockaddr_in6*)&address)->sin6_addr),
             host,
             new_socket);
          ip_address = host;
          */
          assert(!"FIXME!");
       }
    
   //ScreenMessage("ACCEPT:%s",ip_address.c_str());

   bool allowed = false;

   std::string hostname;

   if (feGEMClassType == WORKER) {

      // Only white listed hosts allow on worker
      allowed = fAllowedHosts->IsIPAllowed(ip_address.c_str());
      if (!allowed) {
         ScreenMessage("Blocked");
         fAllowedHosts->PrintRejection(fMfe, ip_address.c_str());
         close(new_socket);
         return;
      }
      // Get hostname from logger
      hostname = std::string(fHistoryLogger.fFullHostName);
      // ScreenMessage("Work IP address allowed :) it belong to %s", hostname);

      // allowed=fAllowedHosts->IsWhiteListed(hostname);
   } else if (feGEMClassType == SUPERVISOR) {
      // Security: Check if host in in allowed host lists (Only do dns lookup as supervisor)
      
      //char servname[200];
      char host[200];
      if (((sockaddr*)&fAddress)->sa_family != AF_INET) {
         std::cout <<"Badness!!! Some IPv6 address that we are not ready for!" <<std::endl;
       }
      int name_status = getnameinfo((const sockaddr*)&fAddress, addrlen, host, 200, 0, 0, 0);
      hostname = std::string(host);
      // Returned 0 on success
      if (name_status) {
         ScreenMessage("Failed to get name_status");
         close(new_socket);
         return;
      }
      // Allow grey listed hosts on supervisor
      allowed = fAllowedHosts->IsAllowed(hostname, ip_address);
      if (!allowed) {
         fAllowedHosts->PrintRejection(fMfe, hostname);
         close(new_socket);
         return;
      }
   }
   // Make sure header memory is clean
   ((GEMBANKARRAY *)fEventBuf)->ClearHeader();
   ((GEMBANK<void *> *)fEventBuf)->ClearHeader();
   bool legal_message = false;
   // Prepare the MIDAS bank so that we can directly write into from the TCP buffer
   fEq->ComposeEvent(fEventBuf, fEventSize);
   fEq->BkInit(fEventBuf, fEventSize);
   // We place the data inside the data bank (aimed minimise move/copy operations for speed)
   // therefor we must define the MIDAS Bankname now, not after we know if its a Generic
   // Equipment Manager bank or Generic Equipment Manager Array GEB1 or GEA1
   char *ptr = (char *)fEq->BkOpen(fEventBuf, "GEM1", TID_STRUCT);
   int read_status = 0;
   int position = 0;
   int BankSize = -1;
   // Get the first chunk of the message (must be atleast the header of the data coming)
   // The header of a GEMBANK is 88 bytes
   // The header of a GEMBANKARRAY is 32 bytes
   // So... the minimum data we need for GetTotalSize() to work is 88 bytes
   int max_reads = 100000;
   int failed_reads = 0;
   while (read_status < 88) {
      usleep(100);
      read_status = read(new_socket, ptr + position, 88 - position);
      // std::cout<<"read:"<<read_status<<"\t"<<position<<std::endl;
      position += read_status;
      if (!position && failed_reads++ == 10)
         break; // Nothing to read...
      if (--max_reads == 0) {
         char message[100];
         sprintf(message, "TCP Read timeout getting bank header");
         {
           std::lock_guard<std::mutex> lock(*fgMutex);
           fMfe->Msg(MTALK, "feGEM", message);
         }
         ScreenMessage("TCP Read timeout getting bank header");
         close(new_socket);
         return;
      }
   }
   // No data to read... does quitting cause a memory leak? It seems not (tested with valgrind)
   if (read_status <= 0) {
      ScreenMessage("Bad read status: %d",read_status);
      fPeriodicity.LogPeriodicWithoutData();
      close(new_socket);
      return;
   } else {
      fEq->WriteStatistics();
      fPeriodicity.LogPeriodicWithData();
   }


   // We have the header... check for compliant data type and get the total size (BankSize)
   if (strncmp(ptr, "GEA1", 4) == 0) {
      GEMBANKARRAY *bank = (GEMBANKARRAY *)ptr;
      BankSize = bank->GetTotalSize();
   } else if (strncmp(ptr, "GEB1", 4) == 0) {
      GEMBANK<void *> *bank = (GEMBANK<void *> *)ptr;
      BankSize = bank->GetTotalSize();
   } // std::cout<<"BankSize:"<<BankSize<<std::endl;
   else {
      char servname[200];
      char host[NI_MAXHOST];
      int name_status;
      {
         std::lock_guard<std::mutex> lock(*fgMutex);
         name_status = getnameinfo((const sockaddr*)&fAddress, addrlen, host, NI_MAXHOST, 0, 0, 0);
      }
      // TODO: If !name_status... throw error listing IP address?
      {
        std::lock_guard<std::mutex> lock(*fgMutex);
        cm_msg(MTALK, "feGEM", "Host %s is sending malformed data... black listing... %d", host, name_status);
      }
      // std::cout<<"Black listing host!"<<std::endl;
      fAllowedHosts->BanHost(host, ip_address);
      legal_message = false;
      close(new_socket);
      return;
   }
   
   if (BankSize > fEventSize)
   {
      char host[200];
      int name_status;
      {
         std::lock_guard<std::mutex> lock(*fgMutex);
         name_status = getnameinfo((const sockaddr*)&fAddress, addrlen, host, 200, 0, 0, 0);
      }
       std::string message = "Data from " + std::string(host) + " bigger than buffer size! Data thrown away ( " + std::to_string(BankSize) + " > " + std::to_string(fEventSize) + ")";
         {
         std::lock_guard<std::mutex> lock(*fgMutex);
         fMfe->Msg(MERROR, "feGEM", message.c_str());
         }
              close(new_socket);
      return;
   }

   const auto tcp_read_start = std::chrono::steady_clock::now();
   // The header looks ok, lets get the whole thing GEMBANK / GEMBANKARRAY
   while (position < BankSize) {
      read_status = read(new_socket, ptr + position, BankSize - position);
      if (!read_status)
         std::this_thread::sleep_for(std::chrono::milliseconds(100));
      position += read_status;
      const auto time_now = std::chrono::steady_clock::now();
      // timeout if TCP read has taken more than 1s
      if (std::chrono::duration_cast<std::chrono::milliseconds>(time_now - tcp_read_start).count() > 1000)
      {
         std::string message = "TCP Read timeout from getting GEMBANKARRAY, got " + std::to_string(position) + " bytes, expected " + std::to_string(BankSize);
         {
         std::lock_guard<std::mutex> lock(*fgMutex);
         fMfe->Msg(MTALK, "feGEM", message.c_str());
         }
         close(new_socket);
         return;
      }
   }
   // std::cout<<BankSize<<"=="<<position<<std::endl;
   if (BankSize == position)
      legal_message = true;
   if (!legal_message)
   {
      char message[100];
      char servname[200];
      char host[200];
      int name_status;
      {
         std::lock_guard<std::mutex> lock(*fgMutex);
         name_status = getnameinfo((const sockaddr*)&fAddress, addrlen, host, 200, 0, 0, 0);
      }
      sprintf(message,"BankSize != position. ( %d != %d ) Bad data from %s",position, BankSize, host);
      std::lock_guard<std::mutex> lock(*fgMutex);
      fMfe->Msg(MTALK, "feGEM", message);
      close(new_socket);
      return;
   }
   //assert(BankSize == position);
   read_status = position;
   int nbanks = 0;
   int nvars = 0;

   // This is a new block of data... clear cache of ODBQueries (used to only 
   // allow one read of unique MIDAS bank per second)
   fODBQueries.clear();

   // Process what we have read into the MIDAS bank
   // printf ("[%s] Received %c%c%c%c (%d bytes)",fEq->fName.c_str(),ptr[0],ptr[1],ptr[2],ptr[3],read_status);
   if (strncmp(ptr, "GEA1", 4) == 0) {
      // std::cout<<"["<<fEq->fName.c_str()<<"] Python / LabVIEW Bank Array found!"<<std::endl;
      std::pair<int,int> vars_banks = HandleBankArray(ptr, hostname, ip_address); // Iterates over array with HandleBank()
      nbanks = vars_banks.second;
      nvars = vars_banks.first;
   } else if (strncmp(ptr, "GEB1", 4) == 0) {
      // std::cout<<"["<<fEq->fName.c_str()<<"] Python / LabVIEW Bank found!"<<std::endl;
      nbanks = HandleBank(ptr, hostname, ip_address);
      nvars = 1;
   } else {
      ScreenMessage("[%s (%s)] Unknown data type just received... \n", fEq->fName.c_str(),ip_address.c_str());
      fMessage.QueueError(fEq->fName.c_str(), "Unknown data type just received... ");
      for (int i = 0; i < 20; i++)
         std::cout << ptr[i];
      exit(1);
   }
   if (!legal_message) {
      close(new_socket);
      return;
   }
   if (nbanks < 0) {
      close(new_socket);
      return;
   }

   fEq->BkClose(fEventBuf, ptr + BankSize);
   fEq->SendEvent(fEventBuf);
   std::chrono::time_point<std::chrono::system_clock> timer_stop = std::chrono::high_resolution_clock::now();
   std::chrono::duration<double, std::milli> handlingtime = timer_stop - timer_start;

   fPeriodicity.AddBanksProcessed(nbanks);

   if (fDebugMode)
      printf(" (debug mode on)\n");

   std::string buf = "DATA OK";
   fMessage.QueueMessage(buf.c_str());

   bool KillFrontend = fMessage.HaveErrors();
   std::vector<char> reply = fMessage.ReadMessageQueue(handlingtime.count());

   ScreenMessage("(%s) Handled %c%c%c%c %d banks (%d bytes) / (%d unique banks) in %fms. Responding with %d bytes",ip_address.c_str(), ptr[0], ptr[1], ptr[2],
                 ptr[3], nbanks, read_status, nvars, handlingtime.count(), reply.size());

   send(new_socket, (char *)&(reply[0]), reply.size(), 0);

   read(new_socket, NULL, 0);
   close(new_socket);

   shutdown(new_socket, SHUT_RD);

   if (KillFrontend) {
      SetFEStatus(-1);
      exit(1);
   }
   return;
}

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
