#include "PeriodicityManager.h"
//--------------------------------------------------
// PeriodicityManager Class
// A class to update the rate at which we poll the Periodic() function
// Data Packers send data once per second...
// By default poll once per second, poll at 1/ Number of connections + 1 seconds
// Ie Match the number of connections and keep one Periodic() per second free for new connections
//--------------------------------------------------

PeriodicityManager::PeriodicityManager(TMFE *mfe, TMFeEquipment *eq, std::mutex *mutex)
{
   
   fPeriod = 500;
   fEq = eq;
   fMfe = mfe;
   fgMutex = mutex;

   fGEMStatLastTime = 0;
   fGEMBankEvents = 0;

   fODBReads = 0;

   fNumberOfConnections = 0;
   fPeriodicWithData = 0;
   fPeriodicWithoutData = 0;
   fOdbStatistics = eq->fOdbEqStatistics;
}

std::string PeriodicityManager::ProgramName(std::string string)
{
   size_t length = string.size();
   for (size_t i = 0; i <= length; i++) {
      if (strncmp((string.c_str() + i), "PROGRAM:", 8) == 0) {
         return string.substr(i + 8, string.size());
      }
   }
   return "Badly formatted string";
}

void PeriodicityManager::AddRemoteCaller(const std::string prog)
{
   for (auto item : RemoteCallers) {
      if (prog == item) {
         std::cout << "Restarted program detected (" << ProgramName(prog) << ")! Total:" << fNumberOfConnections
                   << std::endl;
         fMfe->Msg(MINFO, fEq->fName.c_str(), "Restart of program %s detected", ProgramName(prog).c_str());
         return;
      }
   }
   // Item not already in list
   ++fNumberOfConnections;
   std::cout << "New connection detected (" << ProgramName(prog) << ")! Total:" << fNumberOfConnections << std::endl;
   RemoteCallers.push_back(prog);
}

void PeriodicityManager::LogPeriodicWithData()
{
   TimeOfLastData = std::chrono::high_resolution_clock::now();

   WriteGEMBankStatistics();
   fPeriodicWithData++;
   // Every 1000 events, check that we are have more polls that data being sent
   // (UpdatePerodicity should keep this up to date)
   if (fPeriodicWithData % 1000 == 0) {
      // fPeriodicWithData / fPeriodicWithoutData ~= fNumberOfConnections;
      double EstimatedConnections = (double)fPeriodicWithData / (double)fPeriodicWithoutData;
      double tolerance = 0.1; // 10 percent
      if (EstimatedConnections > fNumberOfConnections * (1 + tolerance)) {
         // The usage of the periodic tasks is beyond spec... perhaps a user didn't initialise connect properly
         //fMfe->Msg(MTALK, fEq->fName.c_str(), "%s periodic tasks are very busy... miss use of the LabVIEW library?",
         //          fEq->fName.c_str());
         fMfe->Msg(MINFO, fEq->fName.c_str(),
                   "Estimated connections:  %d periodics with data /  %d periodics without  > %d fNumberOfConnections",
                   fPeriodicWithData, fPeriodicWithoutData, fNumberOfConnections);
         if (fNumberOfConnections < 5) {
            fNumberOfConnections++;
            UpdatePerodicity();
            fMfe->Msg(MINFO, fEq->fName.c_str(), "Updating periodicity for %d connections?", fNumberOfConnections);
         }
      }
      if (EstimatedConnections < fNumberOfConnections * (1 - tolerance)) {
         std::cout << "Traffic low or inconsistance for fronent:" << fMfe->fFrontendName.c_str() << std::endl;
      }
   }
   std::lock_guard<std::mutex> lock(*fgMutex);
   fOdbStatistics->WI("Periodicicity", fPeriod);
   fOdbStatistics->WI("Connections", fNumberOfConnections);
   fOdbStatistics->WI("PeriodicsWithData", fPeriodicWithData);
   fOdbStatistics->WI("SparePeriodics", fPeriodicWithoutData);
}

void PeriodicityManager::LogPeriodicWithoutData()
{
   fPeriodicWithoutData++;
}

void PeriodicityManager::LogODBRead()
{
   fODBReads++;
}

void PeriodicityManager::AddBanksProcessed(int nbanks)
{
   fGEMBankEvents += nbanks;
}

void PeriodicityManager::WriteGEMBankStatistics()
{
   double now = TMFE::GetTime();
   double elapsed = now - fGEMStatLastTime;

   if (elapsed > 10. || fGEMStatLastTime == 0) {
      fGEMStatLastTime = now;
      std::lock_guard<std::mutex> lock(*fgMutex);
      double GEMBankRate = (double)fGEMBankEvents / elapsed;
      fOdbStatistics->WD("GEM Banks per sec.", GEMBankRate);
      fGEMBankEvents = 0;

      double ODBReadRate = (double)fODBReads / elapsed;
      fOdbStatistics->WD("ODB Reads per sec.", ODBReadRate );
      fODBReads = 0;
   }
   return;
}

void PeriodicityManager::UpdatePerodicity()
{
   std::cout << fPeriod << " > " << 1000. << " / " << (double)fNumberOfConnections << " + 1 " << std::endl;
   if (fPeriod > 1000. / ((double)fNumberOfConnections + 1)) {
      fPeriod = 1000. / ((double)fNumberOfConnections + 1);
      std::cout << "Periodicity increase to " << fPeriod << "ms" << std::endl;
   }
   fPeriodicWithData = 0;
   fPeriodicWithoutData = 0;
}

void PeriodicityManager::ProcessMessage(GEMBANK<char> *bank)
{
   std::cout << "PROCESS:" << bank->GetDataEntry(0)->DATA(0) << std::endl;
   if ((strncmp(bank->GetDataEntry(0)->DATA(0), "New labview connection from", 27) == 0) ||
       (strncmp(bank->GetDataEntry(0)->DATA(0), "New python connection from", 26) == 0)) {
      size_t NameLength = 1023;
      std::string ProgramName = bank->GetDataEntry(0)->DATA(0);
      // Protect against a segfault if program name is very long:
      // trim at 'NameLength' from above
      if (bank->BlockSize - 16 < ProgramName.size())
         ProgramName = ProgramName.substr(0,NameLength);
      AddRemoteCaller(ProgramName);
      UpdatePerodicity();
   }
   return;
}
