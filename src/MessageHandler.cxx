
#include "MessageHandler.h"

//--------------------------------------------------
// Message Handler Class
// A class to queue messages and errors to be sent to labVIEW  (or python) client as JSON
//--------------------------------------------------

MessageHandler::MessageHandler(TMFE *mfe)
{
   fMfe = mfe;
   fTotalText = 0;
}

MessageHandler::~MessageHandler()
{
   if (fJSONMessageQueue.size()) {
      std::cout << "WARNING: Messages not flushed:" << std::endl;
      for (auto msg : fJSONMessageQueue)
         for (char c : msg)
            std::cout << c;
      std::cout << std::endl;
   }
   if (fJSONErrorQueue.size()) {
      std::cout << "ERROR: Errors not flushed:" << std::endl;
      for (auto err : fJSONErrorQueue)
         std::cout << err << std::endl;
   }
}

void MessageHandler::QueueData(const char* name, const int data)
{
   std::lock_guard<std::mutex> lock(fMsgLock);
   std::vector<char> message;
   message.reserve( strlen(name) + 3);
   message.push_back('"');
   for (size_t i = 0; i < strlen(name); i++) {
      message.push_back(name[i]);
   }
   message.push_back('"');
   message.push_back(':');
   std::string data_as_string = std::to_string(data);
   for (const char& c: data_as_string)
   {
      message.push_back(c);
   }
   fJSONMessageQueue.push_back(message);
   fTotalText += message.size();
}

void MessageHandler::QueueString(const char *name, const char *data, int length)
{
   std::lock_guard<std::mutex> lock(fMsgLock);
   if (length < 0)
      length = strlen(data);
   std::vector<char> message;
   message.reserve(length + strlen(name) + 3);
   message.push_back('"');
   for (size_t i = 0; i < strlen(name); i++) {
      message.push_back(name[i]);
   }
   message.push_back('"');
   message.push_back(':');
   message.push_back('"');
   std::string string = ProtectNestedQuotes(data,length);
   for (const char& c: string)
      message.push_back(c);
   message.push_back('"');
   fJSONMessageQueue.push_back(message);
   fTotalText += message.size();
}
// Queue JSON Object
void MessageHandler::QueueFormattedData(const char *name, std::vector<std::string> keys, std::vector<std::string> data)
{
   std::lock_guard<std::mutex> lock(fMsgLock);
   assert(keys.size() == data.size());

   std::vector<char> message;
   int size_estimate = strlen(name) + 3;
   for (const std::string& s: keys)
      size_estimate += s.size() + 3;
   for (const std::string& s: data)
      size_estimate += s.size() + 3;
   message.reserve(size_estimate);
   message.push_back('"');
   for (size_t i = 0; i < strlen(name); i++) {
      message.push_back(name[i]);
   }
   message.push_back('"');
   message.push_back(':');
   message.push_back('{');

   for (size_t i = 0; i < keys.size(); i++) {
      message.push_back('"');
      for (const char& c: keys.at(i)) {
         message.push_back(c);
      }
      message.push_back('"');
      message.push_back(':');
      for (const char& c: data.at(i)) {
         message.push_back(c);
      }
      if (i != keys.size() - 1) {
         message.push_back(',');
      }
   }
   message.push_back('}');
   fJSONMessageQueue.push_back(message);
   //for (const char&c : message)
   //   std::cout << c;
   //std::cout << std::endl;
   fTotalText += message.size();
}
void MessageHandler::QueueMessage(const char *msg)
{
   std::string message = ProtectNestedQuotes(msg);
   QueueString("msg", message.c_str(), message.size());
}

void MessageHandler::QueueError(const char *source, const char *err)
{
   {
     std::lock_guard<std::mutex> lock(fMsgLock);
     fMfe->Msg(MTALK, source, err);
   }
   std::string message = ProtectNestedQuotes(err);
   QueueString("err", message.c_str(), message.size());
}

std::vector<char> MessageHandler::ReadMessageQueue(double midas_time)
{
   // Build basic JSON string ["msg:This is a message to LabVIEW","err:This Is An Error Message"]
   std::lock_guard<std::mutex> lock(fMsgLock);
   std::vector<char> msg;
   msg.reserve(fTotalText + fJSONMessageQueue.size() + fJSONErrorQueue.size() + 1 + 20);
   std::string buf = "{\"MIDASTime\":" + std::to_string(midas_time) +",";
   for (char c : buf)
      msg.push_back(c);
   int i = 0;
   for (auto Message : fJSONMessageQueue) {
      if (i++ > 0)
         msg.push_back(',');
      for (char c : Message)
         msg.push_back(c);
   }
   fJSONMessageQueue.clear();
   for (auto Error : fJSONErrorQueue) {
      if (i++ > 0)
         msg.push_back(',');
      for (char c : Error)
         msg.push_back(c);
   }
   fJSONErrorQueue.clear();
   msg.push_back('}');
   return msg;
}
