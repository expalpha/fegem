//
// feGEM.cxx
//
// Frontend for two way communication to labVIEW (and or python)
// JTK McKENNA
//
#include "feGEMSupervisor.h"

// Amazing, C++ doesn't have a built in case insensitive string compare...
// I built this quickly, its not super optimised
static bool case_insensitive_compare(const std::string& a, const std::string& b)
{
   // Lengths differ... so contents differs...
   if (a.size() != b.size() )
      return false;
   const size_t len = a.size();
   for (size_t i = 0; i < len; i++)
      if ( std::tolower(a[i]) != std::tolower(b[i]))
         return false;
   return true;
}
//--------------------------------------------------
// Base class for LabVIEW frontend
// Child classes:
//    feGEM supervisor (recieves new connections and refers a host to a worker)
//    feGEM worker (one worker per host)
//--------------------------------------------------

feGEMSupervisor::feGEMSupervisor(TMFE *mfe, TMFeEquipment *eq, std::mutex* mutex)
   : feGEMClass(mfe, eq, new AllowedHosts(mfe), mutex, SUPERVISOR) // ctor
{
   fMfe = mfe;
   fEq = eq;
   // Default event size ok 10kb, will be overwritten by ODB entry in Init()
   fEventSize = 10000;
   fEventBuf = NULL;

   HistoryVariable::gHistoryPeriod = 10;
   // So far... limit to 1000 frontend workers...
   fPortRangeStart = 13000;
   fPortRangeStop = 13999;
   // Creating socket file descriptor
   fServer_fd = socket(AF_INET, SOCK_STREAM, 0);
   fcntl(fServer_fd, F_SETFL, O_NONBLOCK);
   if (fServer_fd == 0)
      exit(1);
   // Forcefully attaching socket to the port 5555
   int opt = 1;
   if (setsockopt(fServer_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) {
      // perror("setsockopt");
      exit(1);
   }
   fPort = 12345;
}

void feGEMSupervisor::Init()
{
   std::lock_guard<std::mutex> lock(*fgMutex);
   fEq->fOdbEqSettings->RI("event_size", &fEventSize, true);
   fEq->fOdbEqSettings->RI("port_range_start", &fPortRangeStart, true);
   fEq->fOdbEqSettings->RI("port_range_stop", &fPortRangeStop, true);
   fEq->fOdbEqSettings->RI("DefaultHistoryPeriod", &HistoryVariable::gHistoryPeriod, true);
   assert(fPort > 0);
   fOdbWorkers = fEq->fOdbEqSettings->Chdir("WorkerList", true);

   // Load list of hosts that have logged to MIDAS at some point

   fOdbWorkers->RSA("HostName", &fHostlist, true, 0, 64);
   fOdbWorkers->RSA("IPAddress", &fIplist, true, 0, 64);
   // Loop over all hosts and 'clear' the frontend status
   for (const std::string &host : fHostlist) {
      if (host == "local_host")
         continue;
      // Ignore empty entires
      if (host.empty())
         continue;
      std::string feName = BuildFrontendName(host.c_str());
      std::string status = "feGEM thread not running";
      std::string odb_path = "Equipment/" + feName + "/Common/Status";
      // Check there is an existing entry
      std::string old_status = "";
      fMfe->fOdbRoot->RS(odb_path.c_str(), &old_status);
      // If there is an old entry... set it... else do nothing
      if (old_status.size())
      {
         fMfe->fOdbRoot->WS(odb_path.c_str(), status.c_str());
         odb_path += " color";
         fMfe->fOdbRoot->WS(odb_path.c_str(), "mgray");
      }
   }

   std::vector<uint32_t> DataAdded;
   fOdbWorkers->RU32A("DateAdded", &DataAdded, true, 1);

   if (fEventBuf) {
      free(fEventBuf);
   }
   fEventBuf = (char *)malloc(fEventSize);
   char bind_port[100];
   sprintf(bind_port, "tcp://*:%d", fPort);
   ScreenMessage("Binding to: %s", bind_port);

   // Supervisor must listen to all incoming addresses
   fListen_address.sin_family = AF_INET;
   fListen_address.sin_addr.s_addr = INADDR_ANY;
   fListen_address.sin_port = htons(fPort);

   // Forcefully attaching socket to the listening port (12345)
   if (bind(fServer_fd, (struct sockaddr *)&fListen_address, sizeof(fListen_address)) < 0) {
      ScreenMessage("Bind failed");
      exit(1);
   }
   // assert (rc==0);
   fTCPThread = std::thread(&feGEMClass::Run, this);
}

int feGEMSupervisor::FindHostInWorkerList(const std::string hostname, const std::string ip_address)
{
   // Default entries in worker list


   std::cout<<"Hostname:"<< hostname<<std::endl;
   
   size_t length = hostname.size();
   if (fIplist.size() < length)
       fIplist.resize(length);
   std::cout<<"Length:"<< length << std::endl;
 
   int size = (int)fHostlist.size();
   std::lock_guard<std::mutex> lock(*fgMutex);
   for (int i = 0; i < size; i++) {
      ScreenMessage("%d:%s", i, fHostlist.at(i).c_str());
      if (fHostlist.at(i).size())
      {
         if (case_insensitive_compare (fHostlist.at(i) ,hostname)) {
         //if (strncmp(hostlist.at(i).c_str(), hostname, length) == 0) {
            ScreenMessage("Match found!");
            fOdbWorkers->WSAI("IPAddress", i, ip_address.c_str());
            return i;
         }
      }
   }
   fOdbWorkers->WSAI("HostName", size, hostname.c_str());
   fOdbWorkers->WU32AI("DateAdded", size, (uint32_t)std::time(0));
   fOdbWorkers->WSAI("IPAddress", size, ip_address.c_str());
   ScreenMessage("No Match... return size:%d", size);
   return size;
}

uint16_t feGEMSupervisor::AssignPortForWorker(uint workerID)
{
   std::vector<uint16_t> list;
   ScreenMessage("WorkerID:%d", workerID);
   std::lock_guard<std::mutex> lock(*fgMutex);
   fOdbWorkers->RU16A("Port", &list, true, 0);
   if (workerID >= list.size()) {
      int port = fPort + workerID + 1;
      fOdbWorkers->WU16AI("Port", workerID, port);
      return port;
   } else {
      return list.at(workerID);
   }
}

std::string feGEMSupervisor::BuildFrontendName(const std::string hostname)
{
   std::string name = "feGEM_";
   name += hostname;
   // Remove any non alpha numberic characters
   name.erase(std::remove_if(name.begin(), name.end(),
    [](char c) { return std::isspace(c) || !(std::isalnum(c) || c == '.'  || c == '_' ); } ),
    name.end());

   if (name.size() > (31 - 7)) {
      fMfe->Msg(MERROR, name.c_str(), "Frontend name [%s] too long. Perhaps shorten hostname", name.c_str());
      std::string tmp = name;
      name.clear();
      for (int i = 0; i < (31 - 7); i++) {
         name += tmp[i];
      }
      fMfe->Msg(MERROR, name.c_str(), "Frontend name [%s] too long. Shortenening hostname to [%s]", tmp.c_str(),
                name.c_str());
      // exit(1);
   }
   return name;
}
const char *feGEMSupervisor::AddNewClient(const std::string hostname, const std::string ip_address)
{
   ScreenMessage("Adding host:%s", hostname.c_str());
   ScreenMessage("Check list of workers");
   int WorkerNo = FindHostInWorkerList(hostname, ip_address);
   int port = AssignPortForWorker(WorkerNo);
   ScreenMessage("Assign port %d for worker %d", port, WorkerNo);
   if (!WorkerIsRunning(WorkerNo)) {
      WorkerStarted(WorkerNo);
      // allowed_hosts->AddHost(hostname);
      std::string name; 
      {
         std::lock_guard<std::mutex> lock(*fgMutex);
         name = BuildFrontendName(hostname);
      }
      TMFE *mfe = fMfe;

      TMFeCommon *common;
      {
         std::lock_guard<std::mutex> lock(*fgMutex);
         common = new TMFeCommon();
         common->EventID = 1;
         common->LogHistory = 1;
      }
      // TMFeEquipment* worker_eq = new TMFeEquipment(mfe, name.c_str(), name.c_str(), common);
      TMFeEquipment *worker_eq = new TMFeEquipment(mfe, name.c_str(), common);
      // worker_eq->SetStatus("Starting...", "white");
      {
         std::lock_guard<std::mutex> lock(*fgMutex);
         worker_eq->Init();
         worker_eq->ZeroStatistics();
         worker_eq->WriteStatistics();
         mfe->RegisterEquipment(worker_eq);
      }

      feGEMWorker *workerfe = new feGEMWorker(mfe, worker_eq, fAllowedHosts, fgMutex, hostname);
      workerfe->fPort = port;
      mfe->RegisterRpcHandler(workerfe);
      workerfe->Init(fEq->fOdbEqSettings);
      mfe->RegisterPeriodicHandler(worker_eq, workerfe);
      // mfe->StartRpcThread();
      // mfe->StartPeriodicThread();
      // mfe->StartPeriodicThreads();
      SetFEStatus();
      return "New Frontend started";
   }
   return "Frontend already running";
}
