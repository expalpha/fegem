#include "AllowedHosts.h"
//--------------------------------------------------
// Allowed Hosts class
//          Contents: Listed allowed hosts, grey listed hosts and banned hosts
//     |-->Host class:
//          Contents: Hostname, Rejection Counter and Last Contact time
// Thread safe class to monitor host permissions
//--------------------------------------------------
AllowedHosts::Host::Host(const std::string hostname, const std::string ip_address) : HostName(hostname), IPAddress(ip_address)
{
   RejectionCount = 0;
   LastContact = std::chrono::high_resolution_clock::now();
}

double AllowedHosts::Host::TimeSince(std::chrono::time_point<std::chrono::system_clock> t)
{
   return std::chrono::duration<double, std::milli>(t - LastContact).count();
}

double AllowedHosts::Host::TimeSinceLastContact()
{
   return TimeSince(std::chrono::high_resolution_clock::now());
}

void AllowedHosts::Host::print()
{
   std::cout << "HostName:\t" << HostName << " (" << IPAddress << ")\n";
   std::cout << "RejectionCount:\t" << RejectionCount << "\n";
   std::cout << "Last rejection:\t" << TimeSinceLastContact() * 1000. << "s ago" << std::endl;
}

AllowedHosts::AllowedHosts(TMFE *mfe) : cool_down_time(1000), retry_limit(10)
{
   // Set cooldown time to 10 seconds
   // Set retry limit to 10
   fOdbEqSettings =
      mfe->fOdbRoot->Chdir((std::string("Equipment/") + mfe->fFrontendName + std::string("/Settings")).c_str());
   allow_self_registration = false;
   fOdbEqSettings->RB("allow_self_registration", &allow_self_registration, true);
   std::vector<std::string> hosts;
   std::vector<std::string> ips;
   // Add example first entry
   hosts.push_back("local_host");
   ips.push_back("127.0.0.1");
   fOdbEqSettings->RSA("allowed_hosts", &hosts, true, 10, 64);
   fOdbEqSettings->RSA("allowed_host_ips", &ips, true, hosts.size(), 64);
   if (ips.size() < hosts.size())
      ips.resize( hosts.size());
   // Copy list of good hostnames into array of Host objects
   for (size_t i = 0; i < hosts.size(); i++) {
      if (hosts.at(i).size()) {
         allowed_hosts.push_back(Host(hosts.at(i).c_str(), ips.at(i).c_str()));
      }
   }
   // Check if there enough blank spaces for 10 more hosts (autogrow)
   if (hosts.size() < allowed_hosts.size() + 10) {
      fOdbEqSettings->WSAI("allowed_hosts", allowed_hosts.size() + 10, "");
      fOdbEqSettings->WSAI("allowed_host_ips", allowed_hosts.size() + 10, "");
   }
   hosts.clear(); // Reuse list vector
   ips.clear();

   // Add example first entry
   hosts.push_back("bad_host_name");
   hosts.push_back("bad.host.ip.addr");
   fOdbEqSettings->RSA("banned_hosts", &hosts, true, 10, 64);
   fOdbEqSettings->RSA("banned_host_ips", &ips, true, hosts.size(), 64);

   // Copy bad of good hostnames into array of Host objects
   for (size_t i = 0; i < hosts.size(); i++) {
      if (hosts.at(i).size()) {
         banned_hosts.push_back(Host(hosts.at(i).c_str(), ips.at(i).c_str()));
      }
   }
   // Check if there enough blank spaces for 10 more hosts (autogrow)
   if (hosts.size() < banned_hosts.size() + 10) {
      fOdbEqSettings->WSAI("banned_hosts", banned_hosts.size() + 10, "");
      fOdbEqSettings->WSAI("banned_host_ips", banned_hosts.size() + 10, "");
   }
}

void AllowedHosts::PrintRejection(TMFE *mfe, const std::string hostname)
{
   for (auto &host : banned_hosts) {
      if (host == hostname) {
         if (host.RejectionCount < 2 * retry_limit)
            mfe->Msg(MERROR, "tryAccept", "rejecting connection from unallowed host \'%s\'", hostname.c_str());
         if (host.RejectionCount == 2 * retry_limit)
            mfe->Msg(MERROR, "tryAccept",
                     "rejecting connection from unallowed host \'%s\'. This message will now be suppressed", hostname.c_str());
         host.RejectionCount++;
         return;
      }
   }
}

bool AllowedHosts::IsAllowed(const std::string hostname, const std::string ip_address)
{
   // std::cout<<"Testing:"<<hostname<<std::endl;
   if (IsListedAsAllowed(hostname, ip_address))
      return true;
   if (IsListedAsBanned(hostname))
      return false;
   // Questionable list only permitted if allowing self registration
   if (!allow_self_registration)
      return false;
   if (IsListedAsQuestionable(hostname))
      return true;
   // I should never get this far:
   assert("LOGIC_FAILED");
   return false;
}

bool AllowedHosts::IsIPAllowed(const std::string ip_address)
{
   // std::cout<<"Looking for IP Address:"<<ip_address<<std::endl;
   std::lock_guard<std::mutex> lock(list_lock);
   for (auto &host : allowed_hosts) {
      // host.print();
      if (host.IPAddress == ip_address)
         return true;
   }
   return false;
}

// Check if hostname is allowed, then add the ip_address if so (this should keep up with dynamic IP addresses)
bool AllowedHosts::IsListedAsAllowed(const std::string hostname, const std::string ip_address)
{
   // std::cout<<"Looking for host:"<<hostname<<std::endl;
   std::lock_guard<std::mutex> lock(list_lock);
   //Check for host without a wild card match
   for (auto &host : allowed_hosts) {
      // host.print();
      if (host.ContainsWildCard())
         continue;
      if (host == hostname) {
         host.IPAddress = ip_address;
         return true;
      }
   }
   // No match found... check again but allow wild card matches
   for (auto &host : allowed_hosts) {
      // host.print();
      if (host == hostname) {
         allowed_hosts.push_back(Host(hostname, ip_address));
         return true;
      }
   }
   return false;
}
// Allow this host:
bool AllowedHosts::AddHost(const std::string hostname, const std::string ip_address)
{
   if (!IsListedAsAllowed(hostname, ip_address)) {
      { // Lock guard
         std::lock_guard<std::mutex> lock(list_lock);
         allowed_hosts.push_back(Host(hostname, ip_address));
      } // Lock guard
      std::cout << "Updating ODB with:" << hostname << " at " << (int)allowed_hosts.size() - 1 << std::endl;
      fOdbEqSettings->WSAI("allowed_hosts", (int)allowed_hosts.size() - 1, hostname.c_str());
      fOdbEqSettings->WSAI("ip_address", (int)allowed_hosts.size() - 1, ip_address.c_str());
      // True for new item added
      return true;
   }
   // False, item not added (already in list)
   return false;
}
// Ban this host:
bool AllowedHosts::BanHost(const std::string hostname, const std::string ip_address)
{
   if (!IsListedAsBanned(hostname)) {
      { // thread lock
         std::lock_guard<std::mutex> lock(list_lock);
         banned_hosts.push_back(Host(hostname, ip_address));
         fOdbEqSettings->WSAI("banned_hosts", banned_hosts.size() - 1, hostname.c_str());
         fOdbEqSettings->WSAI("banned_host_ips", banned_hosts.size() - 1, ip_address.c_str());
      } // thread lock
      return true;
   }
   return false;
}

bool AllowedHosts::IsListedAsBanned(const std::string hostname)
{
   const std::lock_guard<std::mutex> lock(list_lock);
   if (!banned_hosts.size())
      return false;
   for (auto &host : banned_hosts) {
      if (host == hostname)
         return true;
   }
   return false;
}

bool AllowedHosts::IsListedAsQuestionable(const std::string hostname)
{
   const std::lock_guard<std::mutex> lock(list_lock);
   for (auto &host : questionable_hosts) {
      if (host == hostname) {
         // host.print();
         std::cout << "Rejection count:" << host.RejectionCount << std::endl;
         if (host.RejectionCount > retry_limit) {
            std::cout << "Banning host " << hostname << std::endl;
            banned_hosts.push_back(host);
            questionable_hosts.remove(host);
         }

         std::chrono::time_point<std::chrono::system_clock> time_now = std::chrono::high_resolution_clock::now();
         std::cout << host.TimeSince(time_now) << ">" << cool_down_time << std::endl;
         if (host.TimeSince(time_now) > cool_down_time) {
            std::cout << "I've seen this host before, but " << host.TimeSince(time_now) / 1000.
                      << " seconds a long time ago" << std::endl;
            host.LastContact = time_now;
            host.RejectionCount++;
            return true;
         } else {
            std::cout << "This host has tried to connect too recently" << std::endl;
            return false;
         }
      }
   }
   // This is the first time a host has tried to connect:
   questionable_hosts.push_back(Host(hostname, ""));
   return true;
}
